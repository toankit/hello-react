Hello World example in React
To run this project in your local setup follow the below steps. 
Go to your GIT bash/terminal and execute the below commands

git clone https://gitlab.com/toankit/hello-react.git
cd hello-react
npm install
npm start

once the server started go to 

http://localhost:8080/

there you can see the results

https://gitlab.com/toankit/hello-react/wikis/Hello-React